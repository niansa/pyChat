# pyChat

An example project on how to use Boost ASIO as a TCP server providing a simple chat service you can connect to just using something like `netcat`.

The port is hardcoded to `22080`.
